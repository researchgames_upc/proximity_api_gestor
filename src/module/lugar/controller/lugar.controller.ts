import {
    Body,
    Controller,
    Get,
    Param,
    ParseIntPipe,
    Post,
    Delete,
    Put,
    UseGuards,
    Patch,
    
  } from '@nestjs/common';
import {  ApiTags } from '@nestjs/swagger';
import {  InjectRolesBuilder, RolesBuilder } from 'nest-access-control';
import { AppResource} from 'src/app.roles';
import { Auth } from 'src/commom/decorators/auth.decarator';
import { Recomendado } from 'src/commom/decorators/recomendado.decorator';
import { User } from 'src/commom/decorators/user.decorator';
import { Lugar } from 'src/entity/lugar.entity';
import { Usuario } from 'src/entity/user.entity';
import { LugarDto } from '../dtos/lugar.dto';
import { LugarService } from '../services/lugar.service';
@ApiTags('Lugar routes')
@Controller('lugar')
export class LugarController {
    constructor(private readonly lugarService: LugarService,
    @InjectRolesBuilder()
    private readonly rolesBuilder: RolesBuilder) {}

    @Get()
    async gettAll() {
      return this.lugarService.getAll();
    }
  
    @Get(':id')
    async getOne(@Param('id', ParseIntPipe) id: number) {
      return await this.lugarService.findById(id);
    }
    //Permite publicar solo a los administradores 
    @Auth(
      {
        possession:'own',
        action:'create',
        resource: AppResource.lugar 
      }
    )
    @Post()
    async create(@Body() dto: LugarDto, @User() user: Usuario) {
      let data;
        data= await this.lugarService.create(dto);
      return data;
    }
     /*Permite editar cualquier Lugar a los administradores ( se puede agregar la opción para que solo puedan
     editar los lugares publicados por ellos mismos)
      */
    @Auth(
      {
        possession:'any',
        action:'update',
        resource: AppResource.lugar 
      }
    )
    @Put(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() dto: LugarDto) {
      return await this.lugarService.update(id, dto);
    }
    @Auth(
      {
        possession:'any',
        action:'delete',
        resource: AppResource.lugar 
      }
    )
    /*Permite eliminar cualquier Lugar a los administradores ( se puede agregar la opción para que solo puedan
     eliminar los lugares publicados por ellos mismos)
    */
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
      return await this.lugarService.delete(id);
    }
    @Auth(
      {
        possession:'any',
        action:'update',
        resource: AppResource.lugar 
      }
    )
    @Patch(':id')
    async updateState(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: LugarDto,
    @Recomendado() Lugar:Lugar) {
      return await this.lugarService.updatePartial(id, dto,Lugar);
      
    }
}

import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsNotEmpty, IsString, ValidateIf } from "class-validator";

export class LugarDto {
    idLugar?: number;
    @ApiProperty()
    @ValidateIf(o=>o.nombreLugar===null)
    @IsString()
    nombreLugar?: string;
    @ApiProperty()
    @ValidateIf(o=>o.estadoLugar===null)
    @IsBoolean()
    estadoLugar?: boolean;
    @ApiProperty()
    @ValidateIf(o=>o.latitud===null)
    @IsString()
    latitud?: string;
    @ApiProperty()
    @ValidateIf(o=>o.longitud===null)
    @IsString()
    longitud?: string;
  }
  
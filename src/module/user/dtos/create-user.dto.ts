import { ApiProperty } from "@nestjs/swagger";
import {  IsArray, IsBoolean, IsEmail, IsEnum, isEnum, IsNotEmpty,IsString, ValidateIf } from "class-validator";
import { AppRoles } from "src/app.roles";

export class CreateUserDto {
    idUsuario?: number;
    @ApiProperty()
    @IsString()
    @ValidateIf(o=>o.nombreUsuario===null)
    nombreUsuario?: string;
    @ApiProperty()
    @ValidateIf(o=>o.apellidoUsuario===null)
    @IsString()
    apellidoUsuario?: string;
    @ApiProperty()
    @ValidateIf(o=>o.emailUsuario===null)
    @IsEmail()
    emailUsuario?: string;
    @ApiProperty()
    @ValidateIf(o=>o.passwordUsuario===null)
    @IsString()
    passwordUsuario?: string;
    @ApiProperty()
    @ValidateIf(o=>o.estado===null)
    @IsBoolean()
    estado:boolean;
    @ApiProperty()
    @ValidateIf(o=>o.roles===null)
    @IsArray()
    @IsEnum(AppRoles,{
        each:true
    })
    roles: string[];
}

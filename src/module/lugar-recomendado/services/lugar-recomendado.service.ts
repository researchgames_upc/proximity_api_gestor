import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Geometry, Point } from 'geojson';
import { lugarRecomendado } from 'src/entity/lugarRecomendado.entity';
import { Repository,QueryBuilder, Connection } from 'typeorm';
import { LugarRecomendadoDto } from '../dtos/lugar-recomendado.dto';
@Injectable()
export class LugarRecomendadoService {
    constructor(
        @InjectRepository(lugarRecomendado)
        private lugarRecomendadoRepository: Repository<lugarRecomendado>,
      ) {}
    
      async getAll(): Promise<lugarRecomendado[]> {
        const list = await this.lugarRecomendadoRepository.find();
        if (!list.length) {
          throw new NotFoundException({ mesage: 'la lista está vacía' });
        }
        return list;
      }
      async findById(id: number,lugarRecomendadoEntity?:lugarRecomendado): Promise<lugarRecomendado> {
        const lugarrecomendado = await this.lugarRecomendadoRepository.findOne(id)
        .then(u => (!lugarRecomendadoEntity ? u : !!u && lugarRecomendadoEntity.idLugarRecomendado === u.idLugarRecomendado ? u : null));
        if (!lugarrecomendado) {
          throw new NotFoundException({ mesage: 'no existe' });
        }
        return lugarrecomendado;
      }
      async findByNombre(nombre: string): Promise<lugarRecomendado> {
        const lugarrecomendado = await this.lugarRecomendadoRepository.findOne({ nombreLugar: nombre });
        return lugarrecomendado;
      }
      
      async create(dto: LugarRecomendadoDto): Promise<any> {
        const lugarrecomendado = await this.lugarRecomendadoRepository.create(dto);
       const cord=`POINT(${lugarrecomendado.longitud} ${lugarrecomendado.latitud})`;
      lugarrecomendado.location = cord;
    
        await this.lugarRecomendadoRepository.save(lugarrecomendado);
        return { message: `lugar ${lugarrecomendado.nombreLugar} creado` };
      }
    
      async update(id: number, dto: LugarRecomendadoDto): Promise<any> {
        const lugarrecomendado = await this.findById(id);
        dto.nombreLugar
          ? (lugarrecomendado.nombreLugar = dto.nombreLugar)
          : (lugarrecomendado.nombreLugar = lugarrecomendado.nombreLugar);
        dto.estadoLugar
          ? (lugarrecomendado.estadoLugar = dto.estadoLugar)
          : (lugarrecomendado.estadoLugar = lugarrecomendado.estadoLugar);
        dto.latitud
          ? (lugarrecomendado.latitud = dto.latitud)
          : (lugarrecomendado.latitud = lugarrecomendado.latitud);
        dto.longitud
          ? (lugarrecomendado.longitud = dto.longitud)
          : (lugarrecomendado.longitud = lugarrecomendado.longitud);
        dto.descripcion
          ?(lugarrecomendado.descripcion=dto.descripcion)
          :(lugarrecomendado.descripcion=lugarrecomendado.descripcion);
        dto.imagen
          ?(lugarrecomendado.imagen=dto.imagen)
          :(lugarrecomendado.imagen=lugarrecomendado.imagen);
        if(dto.longitud&&dto.latitud){
          const cord=`POINT(${lugarrecomendado.longitud} ${lugarrecomendado.latitud})`;
          lugarrecomendado.location = cord;
        }
        await this.lugarRecomendadoRepository.save(lugarrecomendado);
        return { message: `lugar ${lugarrecomendado.nombreLugar} actualizado` };
      }
      async delete(id: number): Promise<any> {
        const lugarrecomendado = await this.findById(id);
        await this.lugarRecomendadoRepository.remove(lugarrecomendado);
        return { message: `lugar ${lugarrecomendado.nombreLugar} eliminado` };
      }
      async updatePartial(id: number, dto: LugarRecomendadoDto,lugarRecomendadoEntity?:lugarRecomendado): Promise<any> {
        const lugarrecomendado = await this.findById(id,lugarRecomendadoEntity);
        const updatedlugar={...lugarrecomendado}
        updatedlugar.estadoLugar=dto.estadoLugar;
        return await this.lugarRecomendadoRepository.save(updatedlugar);
        //return { message: `usuario ${usuario.nombreUsuario} actualizado` };
      }
       async getRange(lat:number,long:number,range:number): Promise<any> { 
       let  locations = await this.lugarRecomendadoRepository
            .createQueryBuilder('lrecomendado')
            .select('lrecomendado.nombreLugar','nombreLugar')
            .addSelect('lrecomendado.estadoLugar','estadoLugar')
            .addSelect('lrecomendado.latitud','latitud')
            .addSelect('lrecomendado.longitud','longitud')
            .addSelect('lrecomendado.descripcion','descripcion')
            .addSelect('lrecomendado.imagen','imagen')
            //.addSelect(`ST_Distance(ST_SRID(POINT(${long},${lat}),3857),lrecomendado.location)*100000`,'distance')
            .where(`ST_Distance(ST_SRID(POINT(${long},${lat}),3857),lrecomendado.location)*100000<${range}`)
            .andWhere(`ST_Distance(ST_SRID(POINT(${long},${lat}),3857),lrecomendado.location)*100000<>0`)
            .andWhere(`lrecomendado.estadoLugar=true`)
            .getRawMany();
  
        return locations;
      }
    }
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { lugarRecomendado } from 'src/entity/lugarRecomendado.entity';
import { LugarRecomendadoController } from './controller/lugar-recomendado.controller';
import { LugarRecomendadoService } from './services/lugar-recomendado.service';

@Module({
  imports:[
    TypeOrmModule.forFeature([lugarRecomendado])
  ],
  controllers: [LugarRecomendadoController],
  providers: [LugarRecomendadoService],
  exports:[LugarRecomendadoService]
})
export class LugarRecomendadoModule {}

import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsNotEmpty, IsString, ValidateIf } from "class-validator";
import { Geometry, Point } from 'geojson';
export class LugarRecomendadoDto {
    idLugarRecomendado?: number;
    @ApiProperty()
    @ValidateIf(o=>o.nombreLugar===null)
    @IsString()
    nombreLugar?: string;
    @ApiProperty()
    @ValidateIf(o=>o.estadoLugar===null)
    @IsBoolean()
    estadoLugar?: boolean;
    @ApiProperty()
    @ValidateIf(o=>o.latitud===null)
    @IsString()
    latitud?: number;
    @ApiProperty()
    @ValidateIf(o=>o.longitud===null)
    @IsString()
    longitud?: number;
    @ApiProperty()
    @ValidateIf(o=>o.descripcion===null)
    @IsString()
    descripcion?: string;
    @ApiProperty()
    @ValidateIf(o=>o.imagen===null)
    @IsString()
    imagen?: string;

  }
  
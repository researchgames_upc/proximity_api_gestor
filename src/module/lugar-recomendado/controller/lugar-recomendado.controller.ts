import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Delete,
  Put,
  Patch,
  ParseFloatPipe,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { InjectRolesBuilder, RolesBuilder } from 'nest-access-control';
import { AppResource } from 'src/app.roles';
import { Auth } from 'src/commom/decorators/auth.decarator';
import { Recomendado } from 'src/commom/decorators/recomendado.decorator';
import { User } from 'src/commom/decorators/user.decorator';
import { lugarRecomendado } from 'src/entity/lugarRecomendado.entity';
import { Usuario } from 'src/entity/user.entity';
import { LugarRecomendadoDto } from '../dtos/lugar-recomendado.dto';
import { LugarRecomendadoService } from '../services/lugar-recomendado.service';
@ApiTags('Recomendado routes')
@Controller('lugarRecomendado')
export class LugarRecomendadoController {   
     constructor(private readonly  lugarRecomendadoService: LugarRecomendadoService,
    @InjectRolesBuilder()
    private readonly rolesBuilder: RolesBuilder) {}
    @Auth(
        {
          possession:'own',
          action:'read',
          resource: AppResource.lugarRecomendado 
        }
      )
    @Get()
    async gettAll() {
      return this.lugarRecomendadoService.getAll();
    }
    @Auth(
        {
          possession:'own',
          action:'read',
          resource: AppResource.lugarRecomendado 
        }
      )
    @Get(':id')
    async getOne(@Param('id', ParseIntPipe) id: number) {
      return await this.lugarRecomendadoService.findById(id);
    }
    @Auth(
      {
        possession:'own',
        action:'create',
        resource: AppResource.lugarRecomendado 
      }
    )
    @Post()
    async create(@Body() dto: LugarRecomendadoDto, @User() user: Usuario) {
      let data;
        data= await this.lugarRecomendadoService.create(dto);
      return data;
    }
    @Auth(
      {
        possession:'any',
        action:'update',
        resource: AppResource.lugarRecomendado 
      }
    )
    @Put(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() dto: LugarRecomendadoDto) {
      return await this.lugarRecomendadoService.update(id, dto);
    }
    @Auth(
      {
        possession:'any',
        action:'delete',
        resource: AppResource.lugarRecomendado 
      }
    )
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
      return await this.lugarRecomendadoService.delete(id);
    }
    @Auth(
      {
        possession:'any',
        action:'update',
        resource: AppResource.lugarRecomendado 
      }
    )
    @Patch(':id')
    async updateState(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: LugarRecomendadoDto,
    @Recomendado() lugarRecomendado:lugarRecomendado) {
      
      return await this.lugarRecomendadoService.updatePartial(id, dto,lugarRecomendado);
    }
    @Auth(
      {
        possession:'own',
        action:'read',
        resource: AppResource.lugarRecomendado 
      }
    )
    @Post('Near/:range')
    public async getRange(
      @Param('range',ParseIntPipe)range:number,
      @Body() dto: LugarRecomendadoDto
    ){
      return await this.lugarRecomendadoService.getRange(dto.latitud,dto.longitud,range);
    }
 
}


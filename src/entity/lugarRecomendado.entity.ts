import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';
import {  Point } from 'geojson';
@Entity({ name: 'lrecomendado' })
export class lugarRecomendado {
  @PrimaryGeneratedColumn()
  idLugarRecomendado: number;

  @Column({ type: 'varchar', length: 45, nullable: false, unique: true })
  nombreLugar: string;

  @Column({ type: 'bool', default: true })
  estadoLugar: boolean;
  @Column({ type: 'double precision' })
  latitud: number;
  @Column({ type: 'double precision'})
  longitud: number;
  @Column({ type: 'varchar', length: 300, nullable: false })
  descripcion: string;
  @Column({ type: 'varchar', length: 300, nullable: false })
  imagen: string;
  @Index({ spatial: true })
  @Column({
    type: 'point',
    spatialFeatureType: 'Point', 
    srid: 3857,
  
  })
  location:string
}

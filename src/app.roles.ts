import { RolesBuilder } from "nest-access-control";
import { lugarRecomendado } from "./entity/lugarRecomendado.entity";
export enum AppRoles{
    user='user',
    admin='admin'
}
export enum AppResource{
    User='user',
    lugar='lugar',
    lugarRecomendado='lugarRecomendado'
}
export const roles:RolesBuilder= new RolesBuilder();
roles
    // User Roles
    .grant(AppRoles.user)
    .updateOwn(AppResource.User)
    .deleteOwn(AppResource.User)
    .readOwn(AppResource.lugarRecomendado)
    // admin Roles
    .grant(AppRoles.admin)
    .extend(AppRoles.user)
    .createOwn(AppResource.User)
    .updateAny(AppResource.User)
    .deleteAny(AppResource.User)
    .createOwn(AppResource.lugar)
    .updateAny(AppResource.lugar)
    .deleteAny(AppResource.lugar)
    .readAny(AppResource.lugarRecomendado)
    .createOwn(AppResource.lugarRecomendado)
    .updateAny(AppResource.lugarRecomendado)
    .deleteAny(AppResource.lugarRecomendado)